<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transfers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transfer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <h2>Total: <?=$money?> $</h2>
    <div class="row">
        <?php if($transfersSend) : ?>
            <div class="col-md-6">
                <h2>Send</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Receiver</th>
                        <th scope="col">Money</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($transfersSend as $transfer) : ?>
                        <tr>
                            <th scope="row"><?=$transfer->id?></th>
                            <td><?=$transfer->user_receiver?></td>
                            <td style="color: red">- <?=$transfer->money?> $</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>

        <?php if($transfersReceive) : ?>
            <div class="col-md-6">
                <h2>Receive</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Sender</th>
                        <th scope="col">Money</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($transfersReceive as $transfer) : ?>
                        <tr>
                            <th scope="row"><?=$transfer->id?></th>
                            <td><?=$transfer->user_sender?></td>
                            <td style="color: green">+ <?=$transfer->money?> $</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>
