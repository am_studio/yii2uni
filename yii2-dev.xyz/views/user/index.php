<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if($users) : ?>
        <ul class="list-group">
        <?php foreach ($users as $user) : ?>
            <li class="list-group-item">
                <div class="pull-left"><?=$user->username?></div>
                <div class="pull-right"><?=$user->money?> $</div>
                <div class="clearfix"></div>
            </li>
        <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
