<?php

namespace app\controllers;

use app\models\TransferForm;
use Yii;
use app\models\User;
use yii\web\Controller;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $users = User::find()->all();

        return $this->render('index', [
            'users' => $users,
        ]);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionTransfer()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = new TransferForm();
        if ($model->load(Yii::$app->request->post()) && $model->send()) {
            return $this->redirect(['user/index']);
        }
        return $this->render('transfer', [
            'model' => $model,
        ]);
    }

    public function actionHistory()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user = User::findOne(Yii::$app->user->id);

        return $this->render('history', [
            'transfersSend' => $user->transfers,
            'transfersReceive' => $user->receive,
            'money' => $user->money,
        ]);
    }
}
