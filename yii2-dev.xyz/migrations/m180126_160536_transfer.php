<?php

use yii\db\Migration;

/**
 * Class m180126_160536_transfer
 */
class m180126_160536_transfer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('transfer', [
            'id' => $this->primaryKey(),
            'user_sender' => $this->string(128)->notNull(),
            'user_receiver' => $this->string(128)->notNull(),
            'money' => $this->decimal(10, 2),
        ]);

        $this->addForeignKey('transfer_sender_fk', 'transfer', 'user_sender', 'user', 'username', 'CASCADE');
        $this->addForeignKey('transfer_receiver_fk', 'transfer', 'user_receiver', 'user', 'username', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'transfer_sender_fk',
            'transfer'
        );

        $this->dropForeignKey(
            'transfer_receiver_fk',
            'transfer'
        );

        $this->dropTable('transfer');
    }
}
