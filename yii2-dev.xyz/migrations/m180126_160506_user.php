<?php

use yii\db\Migration;

/**
 * Class m180126_160506_user
 */
class m180126_160506_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'username' => $this->string(128)->notNull()->unique(),
            'money' => $this->decimal(10, 2)->defaultValue(0),
            'auth_key' => $this->string(32)->notNull()
        ]);

        $this->addPrimaryKey('pk_user','user', ['username']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
