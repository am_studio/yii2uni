<?php

use app\models\User;

class ExampleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testSomeFeature()
    {
        $this->assertTrue(true);
    }

    public function testCheckSum()
    {
        $users = User::find()->all();
        $sum = 0;
        foreach($users as $user) {
            $sum += $user->money;
        }

        if ($sum == 0) {
            $result = true;
        } else {
            $result = false;
        }

        $this->assertTrue($result);
    }

}