<?php

namespace tests\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        expect_that($user = User::findOne(['username' => 'admin']));
    }

    public function testFindUserByAccessToken()
    {
        expect_that($user = User::findOne(['auth_key' => '100-token']));
        expect_not($user = User::findOne(['auth_key' => 'non-existing']));
    }

    public function testFindUserByUsername()
    {
        expect_that($user = User::findByUsername('admin'));
        expect_not(User::findByUsername('not-admin'));
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = User::findByUsername('admin');
        expect_that($user->validateAuthKey('100-token'));
        expect_not($user->validateAuthKey('test102key'));
    }

}
