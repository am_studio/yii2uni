<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class TransferForm extends Model
{
    public $username = false;
    public $money = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'money'], 'required'],
            ['username', function ($attribute, $params) {
                if ($this->$attribute === Yii::$app->user->id) {
                    $this->addError($attribute, 'You can not send yourself!');
                }
            }],
            ['money', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function send()
    {
        // Check valid POST
        if ($this->validate()) {
            $userSender = User::findOne(Yii::$app->user->id);
            $userSender->money -= $this->money;
            $userSender->save();

            // Check if exist
            if($resultReceiver = User::findOne($this->username)) {
                $resultReceiver->money += $this->money;
                $resultReceiver->save();
            } else {
                // Create new User
                $userReceiver = new User();
                $userReceiver->username = $this->username;
                $userReceiver->money = $this->money;
                $userReceiver->save();
            }

            // Create transfer
            $transfer = new Transfer();
            $transfer->user_sender = $userSender->username;
            $transfer->user_receiver = $this->username;
            $transfer->money = $this->money;
            $transfer->save();
            return true;
        }
        return false;
    }
}
