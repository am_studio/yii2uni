<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transfer".
 *
 * @property string $user_sender
 * @property string $user_receiver
 * @property string $money
 */
class Transfer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transfer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_sender', 'user_receiver'], 'required'],
            [['money'], 'number'],
            [['user_sender', 'user_receiver'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_sender' => 'User Sender',
            'user_receiver' => 'User Receiver',
            'money' => 'Money',
        ];
    }
}
